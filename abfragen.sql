USE anigram;

-- 1, Alle Post aufrufen, nach Likes sortiert
CREATE OR REPLACE VIEW all_posts_ordered_by_count_of_likes AS
SELECT title, img, text, ( SELECT COUNT(*) FROM likes l WHERE l.posts_idfs = p.id ) as Likes FROM Posts p
WHERE p.deactivated = 0
ORDER BY Likes DESC;

-- 2, Alle Post von den Freunden von user1
SELECT title, img, text FROM Posts p
INNER JOIN friends AS f 
WHERE p.usr_idfs = f.usr_idfs2 
AND f.usr_idfs1 = 1
AND p.deactivated = 0;
                    
-- 3, Alle Post die user1 geliket hat
CREATE OR REPLACE VIEW posts_user1_has_liked AS
SELECT title, img, text FROM Posts p
INNER JOIN likes l ON l.posts_idfs = p.id
WHERE l.usr_idfs = 1 AND p.deactivated = 0;

-- 4, Wenn ein Post mehr als 5 Meldungen hat sollte es deaktiviert werden
UPDATE posts p
SET p.deactivated = 1
WHERE (SELECT COUNT(*) FROM report r WHERE r.posts_idfs = p.id) > 5;

-- 5, Alle Clubs sortiert nach Mitgliedernazahl
CREATE OR REPLACE VIEW clubs_ordered_by_count_of_users AS
SELECT c.name, c.description, COUNT(f.id) as followers FROM followclub AS f
INNER JOIN club c ON c.id = f.cat_idfs
GROUP BY c.name DESC;

-- 6, Alle posts von deaktivierten usern
CREATE OR REPLACE VIEW posts_of_deactivatedd_users AS
SELECT title, img, text, p.deactivated, a.username FROM posts AS p
INNER JOIN account AS a
WHERE p.deactivated = 1 AND p.usr_idfs = a.id;

-- 7, Top 5 Users mit den meisten posts
CREATE OR REPLACE VIEW Users_with_most_posts AS
SELECT username, firstname, lastname, (SELECT COUNT(*) FROM posts AS p WHERE p.usr_idfs = a.id AND p.deactivated = 0) AS posts_count FROM account as a
ORDER BY posts_count DESC
LIMIT 5;

-- 8, Alle usernamen der users, die gerade online sind
CREATE OR REPLACE VIEW Online_Users AS
SELECT username FROM `account` WHERE is_online = 1;