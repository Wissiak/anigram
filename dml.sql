USE anigram;

-- account
INSERT INTO account (username,firstname,lastname,password,birthday,email,pic,deactivated, is_online) VALUES
( 'nico.php', 'Nico', 'Lutz', 'd3751d33f9cd5049c4af2b462735457e4d3baf130bcbb87f389e349fbaeb20b9', '1999-06-21', 'nico.lutz@gmail.com', 'bild.png', 0, 1),
( 'theRealJay', 'Gabriel', 'Meier', 'd3751d33f9cd5049c4af2b462735457e4d3baf130bcbb87f389e349fbaeb20b9', '1997-12-20', 'gabriel.meier@gmail.com', 'bild.png', 0, 0),
( 'padi_pada', 'Patrick', 'Wissiak', 'd3751d33f9cd5049c4af2b462735457e4d3baf130bcbb87f389e349fbaeb20b9', '1999-11-02', 'patrick.wissiak@gmail.com', 'bild.png', 0, 0),
( 'BurntRice', 'Marc', 'Vollenweider', 'd3751d33f9cd5049c4af2b462735457e4d3baf130bcbb87f389e349fbaeb20b9', '1999-04-06', 'marc.vollenweider@gmail.com', 'bild.png', 0, 0),
( 'ViVa', 'Vithun', 'Vamathevan', 'd3751d33f9cd5049c4af2b462735457e4d3baf130bcbb87f389e349fbaeb20b9', '1998-12-14', 'vithun.vamathevan@gmail.com', 'bild.png', 0, 0),
( 'JavaCoffee', 'Serge', 'Roth', 'd3751d33f9cd5049c4af2b462735457e4d3baf130bcbb87f389e349fbaeb20b9', '1998-06-26', 'serge.roth@gmail.com', 'bild.png', 0, 0),
( 'TheAuthor', 'Yvo', 'Keller', 'd3751d33f9cd5049c4af2b462735457e4d3baf130bcbb87f389e349fbaeb20b9', '1999-06-01', 'yvo.keller@gmail.com', 'bild.png', 0, 0),
( 'FastAndFurious98', 'Dennis', 'Schäppi', 'd3751d33f9cd5049c4af2b462735457e4d3baf130bcbb87f389e349fbaeb20b9', '1998-05-19', 'dennis.schaeppi@gmail.com', 'bild.png', 0, 0),
( 'AniBanani', 'Anna', 'Lutz', 'd3751d33f9cd5049c4af2b462735457e4d3baf130bcbb87f389e349fbaeb20b9', '1998-06-21', ' anna.lutz@gmail.com', 'bild.png', 0, 1),
( 'theRealG', 'Alex', 'Maurer', 'd3751d33f9cd5049c4af2b462735457e4d3baf130bcbb87f389e349fbaeb20b9', '1999-05-26', 'alex.mauerer@gmail.com', 'bild.png', 1, 0);

-- feedback
INSERT INTO feedback (comment,usr_idfs) VALUES
( 'Schöni Datebank hend ihr. Möchti au so eine', 1),
( 'Anigram heisst fast wie ich.', 9),
( 'Wieso bin ich gsperrt?', 10);

-- friends
INSERT INTO friends (usr_idfs1, usr_idfs2) VALUES 
( 1, 9),
( 1, 2),
( 10, 9),
( 1, 4),
( 3, 5),
( 2, 3),
( 3, 4),
( 6, 5),
( 4, 6),
( 2, 9);

-- club
INSERT INTO club (name, description, img) VALUES 
( 'ZLI Gang', 'Wir sind die ehemaligen ZLI Schüler', 'bild.png'),
( 'AniBanani Fanclub', 'Wir lieben Anibanani Lets Plays', 'bild.png');

-- followclub
INSERT INTO followclub (usr_idfs, cat_idfs) VALUES
( 1, 1),
( 2, 1),
( 3, 1),
( 4, 1),
( 5, 1),
( 6, 1);

-- posts
INSERT INTO posts (title, img, text, deactivated, usr_idfs) VALUES
( 'So süss', 'bild.png', 'Mein Haustier', 0, 9),
( '#joke', 'bild.png', 'Richtig lustig', 0, 3),
( 'Lol', 'bild.png', 'Funny', 0, 4),
( 'Zoo', 'bild.png', 'Pinguin', 0, 6),
( 'Troll', 'bild.png', 'Trolololololo', 1, 10);

-- likes
INSERT INTO likes (usr_idfs, posts_idfs) VALUES
( 1, 1),
( 1, 2),
( 1, 3),
( 1, 4),
( 1, 5),
( 2, 3),
( 2, 4),
( 2, 5),
( 3, 1),
( 3, 2),
( 5, 3);

-- comment
INSERT INTO comment (text, usr_idfs, posts_idfs) VALUES
( 'Je t`aime', 1, 1),
( 'Cool', 2, 1),
( 'Läss', 3, 1),
( 'Bro', 4, 1),
( 'Isch guet', 5, 1),
( 'De isch nöd guet', 6, 2),
( 'Heb Fresse', 6, 2);

-- report
INSERT INTO report (reason, posts_idfs) VALUES
( 'Rassistisch', 5),
( 'Offensiv', 5),
( 'Nicht SFW', 5),
( 'Beleidigung', 5),
( 'Ich fühle mich beleidigt', 5),
( 'Please remove', 5);