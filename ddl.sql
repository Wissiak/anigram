--
-- Datenbank: `Anigram`
--
DROP DATABASE IF EXISTS `Anigram`;
CREATE DATABASE IF NOT EXISTS `Anigram`;
USE `Anigram`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `account`
--
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) UNIQUE NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `birthday` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `pic` varchar(255),
  `deactivated` BIT NOT NULL,
  `is_online` BIT NOT NULL,
  PRIMARY KEY (id)
);

--
-- Tabellenstruktur für Tabelle `feedback`
--
DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `comment` text NOT NULL,
  `usr_idfs` int(11) NOT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (usr_idfs) REFERENCES account(id)
);

--
-- Tabellenstruktur für Tabelle `friends`
--
DROP TABLE IF EXISTS `friends`;
CREATE TABLE `friends` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `usr_idfs1` int(11) NOT NULL,
  `usr_idfs2` int(11) NOT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (usr_idfs1) REFERENCES account(id),
  FOREIGN KEY (usr_idfs2) REFERENCES account(id)
);

--
-- Tabellenstruktur für Tabelle `club`
--
DROP TABLE IF EXISTS `club`;
CREATE TABLE `club` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `img` varchar(255),

  PRIMARY KEY (id)
);

--
-- Tabellenstruktur für Tabelle `followclub`
--
DROP TABLE IF EXISTS `followclub`;
CREATE TABLE `followclub` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `usr_idfs` int(11) NOT NULL,
  `cat_idfs` int(11) NOT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (usr_idfs) REFERENCES account(id),
  FOREIGN KEY (cat_idfs) REFERENCES club(id)
);

--
-- Tabellenstruktur für Tabelle `posts`
--
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `deactivated` BIT NOT NULL,
  `usr_idfs` int(11) NOT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (usr_idfs) REFERENCES account(id)
);

--
-- Tabellenstruktur für Tabelle `likes`
--
DROP TABLE IF EXISTS `likes`;
CREATE TABLE `likes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `usr_idfs` int(11) NOT NULL,
  `posts_idfs` int(11) NOT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (usr_idfs) REFERENCES account(id),
  FOREIGN KEY (posts_idfs) REFERENCES posts(id)
);

--
-- Tabellenstruktur für Tabelle `comment`
--
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `usr_idfs` int(11) NOT NULL,
  `posts_idfs` int(11) NOT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (usr_idfs) REFERENCES account(id),
  FOREIGN KEY (posts_idfs) REFERENCES posts(id)
);

--
-- Tabellenstruktur für Tabelle `report`
--
DROP TABLE IF EXISTS `report`;
CREATE TABLE `report` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `reason` text NOT NULL,
  `posts_idfs` int(11) NOT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (posts_idfs) REFERENCES posts(id)
);
